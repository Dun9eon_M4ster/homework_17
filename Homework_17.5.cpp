#include <iostream>
#include <math.h>
using namespace std;

class Vector
{
private:
	double x = 0;
	double y = 0;
	double z = 0;
public:
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{
		cout << x << " " << y << " " << z << endl;
	}
	double Module()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
};

class Storage
{
private:
	int a, b, c;
public:
	Storage()
	{
		a = 11;
		b = 42;
		c = 81;
	}
	
	void PrintA()
	{
		cout << a << " ";
	}

	void PrintB()
	{
		cout << b << " ";
	}

	void PrintC()
	{
		cout << c << " ";
	}
};

int main()
{
	Storage NewClass;
	NewClass.PrintA();
	NewClass.PrintB();
	NewClass.PrintC();

	cout << endl;

	Vector vector(2, 3, 5);
	vector.Show();
	cout << vector.Module();

	return 0;
}
